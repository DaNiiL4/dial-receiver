package com.example.daniil4.dialreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CallReceiver extends BroadcastReceiver {
    private static boolean incomingCall = false;
    private static WindowManager windowManager;
    private static ViewGroup windowLayout;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
            String phoneState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (phoneState.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                //Трубка не поднята, телефон звонит
                String phoneNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                incomingCall = true;
                Log.d(MainActivity.LOG_TAG, "Show window: " + phoneNumber);
                Call call = new Call(phoneNumber);
                call.save();
                showWindow(context, call);

            } else if (phoneState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                //Телефон находится в режиме звонка (набор номера при исходящем звонке / разговор)
                if (incomingCall) {
                    Log.d(MainActivity.LOG_TAG, "Close window.");
                    closeWindow();
                    incomingCall = false;
                }
            } else if (phoneState.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                //Телефон находится в ждущем режиме - это событие наступает по окончанию разговора
                //или в ситуации "отказался поднимать трубку и сбросил звонок".
                if (incomingCall) {
                    Log.d(MainActivity.LOG_TAG, "Close window.");
                    closeWindow();
                    incomingCall = false;
                }
            }
        }
    }

    private void showWindow(Context context, Call call) {
        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP;

        windowLayout = (ViewGroup) layoutInflater.inflate(R.layout.call_dialog, null);

        TextView textViewNumber=(TextView) windowLayout.findViewById(R.id.callNumberDialog);
        TextView textViewDate=(TextView) windowLayout.findViewById(R.id.callDateDialog);
        Button buttonClose=(Button) windowLayout.findViewById(R.id.buttonCloseDialog);
        ImageView imageView = (ImageView) windowLayout.findViewById(R.id.callImageDialog);

        textViewNumber.setText(call.number);
        textViewDate.setText(call.date);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeWindow();
            }
        });

        int img_res = Picture.findById(Picture.class, call.picture_id).resource;
        imageView.setImageResource(img_res);
        windowManager.addView(windowLayout, params);
    }

    private void closeWindow() {
        if (windowLayout != null){
            windowManager.removeView(windowLayout);
            windowLayout = null;
        }
    }
}