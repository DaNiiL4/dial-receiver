package com.example.daniil4.dialreceiver;

import com.orm.SugarRecord;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Random;

public class Call extends SugarRecord {
    String number;
    String date;
    Long picture_id;

    public Call(){
    }

    public Call(String number, String date, Long picture_id) {
        this.number = number;
        this.date = date;
        this.picture_id = picture_id;
    }

    public Call(String number){
        this.number = number;

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        this.date = sdf.format(new Date(System.currentTimeMillis()));

        List<Picture> pictures = Picture.listAll(Picture.class);
        int index = new Random().nextInt(pictures.size());
        this.picture_id = pictures.get(index).getId();
    }
}
