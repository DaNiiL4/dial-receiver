package com.example.daniil4.dialreceiver;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orm.SugarContext;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    final static String LOG_TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SugarContext.init(this);
        if(Picture.count(Picture.class) == 0) {
            List<Picture> books = new ArrayList<>();
            books.add(new Picture(R.drawable.abstr));
            books.add(new Picture(R.drawable.derevo));
            books.add(new Picture(R.drawable.nebo));
            SugarRecord.saveInTx(books);
        }

        LinearLayout linLayout = (LinearLayout) findViewById(R.id.linLayout);
        LayoutInflater ltInflater = getLayoutInflater();

        List<Call> calls = Call.listAll(Call.class);

        for(Call call : calls){
            View item = ltInflater.inflate(R.layout.call_info, linLayout, false);
            TextView callNumberInfo = (TextView) item.findViewById(R.id.callNumberInfo);
            callNumberInfo.setText(call.number);
            TextView callDateInfo = (TextView) item.findViewById(R.id.callDateInfo);
            callDateInfo.setText(call.date);
            ImageView callImageViewInfo = (ImageView) item.findViewById(R.id.callImageViewInfo);
            callImageViewInfo.setImageResource(Picture.findById(Picture.class, call.picture_id).resource);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 20);
            item.setLayoutParams(lp);

            linLayout.addView(item);
        }
    }
}
